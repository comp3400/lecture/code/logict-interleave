{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE LambdaCase #-}

data Logic a =
  Logic (forall r. (a -> r -> r) -> r -> r)

runLogic :: Logic a -> (a -> r -> r) -> r -> r
runLogic (Logic k) = k

instance Functor Logic where
  fmap f (Logic k) =
    Logic (\c n -> k (c . f) n)

instance Applicative Logic where
  pure a = Logic
    (\c n -> c a n)
  Logic f <*> Logic a =
    Logic (\c n -> f (\h t -> a (c . h) t) n)

instance Monad Logic where
  return = pure
  Logic x >>= f = Logic (\c n -> x (\h t -> runLogic (f h) c t) n)

empty :: Logic a
empty = Logic (const id)

cons :: a -> Logic a -> Logic a
cons h (Logic t) = Logic (\c n -> c h (t c n))

logicList :: Logic a -> [a]
logicList (Logic k) = k (:) []

listLogic :: [a] -> Logic a
listLogic = foldr cons empty

(.+.) :: Logic a -> Logic a -> Logic a
l1 .+. l2 = Logic (\c n -> runLogic l1 c (runLogic l2 c n))

reflect :: Maybe (a, Logic a) -> Logic a
reflect Nothing = empty
reflect (Just (h, t)) = pure h .+. t

msplit :: Logic a -> Logic (Maybe (a, Logic a))
msplit (Logic k) = pure (k (\h t -> Just (h, reflect t)) Nothing)

interleave :: Logic a -> Logic a -> Logic a
interleave l1 l2 =
  msplit l1 >>=
    \case
      Nothing ->
        l2
      Just (h, t) ->
        pure h .+. interleave l2 t

odds :: Logic Integer
odds = return 1 .+. fmap (2+) odds

program :: (Logic Integer -> Logic Integer -> Logic Integer) -> Logic Integer
program combine =
  do  x <- odds `combine` return 2
      if even x then return x else empty

headOr :: a -> [a] -> a
headOr = foldr const

example1 :: Integer
example1 = headOr 99 . logicList $ program (.+.)

example2 :: Integer
example2 = headOr 99 . logicList $ program interleave

----

data LogicT k a = LogicT (forall r. (a -> k r -> k r) -> k r -> k r)

runLogicT :: LogicT k a -> (a -> k r -> k r) -> k r -> k r
runLogicT (LogicT k) = k

instance Functor (LogicT k) where
  fmap f (LogicT k) =
    LogicT (\c n -> k (c . f) n)

instance Applicative (LogicT k) where
  pure a = LogicT
    (\c n -> c a n)
  LogicT f <*> LogicT a =
    LogicT (\c n -> f (\h t -> a (c . h) t) n)

instance Monad (LogicT k) where
  return = pure
  LogicT x >>= f = LogicT (\c n -> x (\h t -> runLogicT (f h) c t) n)

